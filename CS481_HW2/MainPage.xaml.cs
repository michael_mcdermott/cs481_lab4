﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

using Xamarin.Forms;

namespace CS481_HW2
{
    [DesignTimeVisible(false)]
    public partial class MainPage
    {
        //Declare global variables for state machine and calculations 
        private int _currentState = 1;
        private string _operator;
        private double _firstNum, _secondNum;


        public MainPage()
        {
            InitializeComponent();
            Clear(this, null);
        }
        //assigns values to display after pressed. determines and reassigns state
        public void SelectNumber(object sender, EventArgs e)
        {
            var button = (Button)sender;
            var selection = button.Text ?? throw new ArgumentNullException(nameof(e));

            if (_currentState >= 2)
            {
                if (this.Result.Text != "-")
                    this.Result.Text = "";
            }

            this.Result.Text += selection;

            if (!double.TryParse(this.Result.Text, out var number)) return;

            this.Result.Text = number.ToString("N0");

            if (_currentState == 1)
            {
                _firstNum = number;
            }
            else
            {
                _secondNum = number;
            }
        }
        //determines state and saves the selected operator in a string
        public void SelectOperator(object sender, EventArgs e)
        {
            _currentState = 2;
            var button = (Button)sender;
            var operatorText = button.Text;
            _operator = operatorText;
        }
        /*concatenates parenthesis
        public void Parenthesis(object sender, EventArgs e)
        {
            
            switch (this.Result.Text)
            {
                case "(":
                    this.Result.Text += "(";
                    break;
                case ")":
                    this.Result.Text += ")";
                    break;
            }
        }
        */
        //Set negative param by string concatenation
        public void SetNegative(object sender, EventArgs e)
        {
            this.Result.Text = "-";
        }
        //determines state and calls calculate method 
        public void Calculate(object sender, EventArgs e)
        {
            if (_currentState != 2) return;
            var result = CalculationStation.Calculate(_firstNum, _secondNum, _operator);

            this.Result.Text = result.ToString(CultureInfo.InvariantCulture);
            _firstNum = result;

        }
        //percentage feature
        public void Percentage(object sender, EventArgs e)
        {
            var result = _firstNum * .01;
            this.Result.Text = result.ToString(CultureInfo.InvariantCulture);
            _currentState = 2;
            _firstNum = result;
        }
        //resets the fields of global variables
        public void Clear(object sender, EventArgs e)
        {
            _firstNum = 0;
            _secondNum = 0;
            _currentState = 1;
            this.Result.Text = "0";
        }
    }
    //Class to handle calculations 
    public static class CalculationStation
    {
        public static double Calculate(double val1, double val2, string operation)
        {
            double result = 0;

            switch (operation)
            {
                case "/":
                    result = val1 / val2;
                    break;
                case "x":
                    result = val1 * val2;
                    break;
                case "-":
                    result = val1 - val2;
                    break;
                case "+":
                    result = val1 + val2;
                    break;
            }

            return result;
        }
    }
}
